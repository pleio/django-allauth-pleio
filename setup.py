from setuptools import setup, find_packages

setup(
    name="django-keycloak-integration",
    version="1.0.0",
    author="Pleio",
    author_email="arjen@pleio.nl",
    description="Keycloak integration",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/pleio/django-keycloak-integration",
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",  # Choose the appropriate license
        "Operating System :: OS Independent",
        "Intended Audience :: Developers",
    ],
    python_requires='>=3.10',   # Specify the minimum required Python version
    install_requires=[
        "Django",
        "requests",
        "pytest"
    ],
)