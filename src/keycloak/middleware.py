import logging
import os
import requests
from django.conf import settings
from django.core.exceptions import PermissionDenied

logger = logging.getLogger(__name__)


def is_saml_path(request):
    return request.path.startswith("/saml/")


def is_callback_path(request):
    return request.path.startswith("/accounts/oidc/keycloak/login/callback/")


def is_reload_path(request):
    return request.path.startswith("/__reload__/")


def is_admin_path(request):
    return request.path.startswith("/admin/")


def is_cms_path(request):
    return request.path.startswith("/cms/")


def authorized_for_admin(request):
    return (
            request.user.is_authenticated
            and request.user.is_staff
            or request.user.is_superuser
    )

def has_cms_role(user):
    user_groups = user.groups.values_list("name", flat=True)
    if any(group in user_groups for k, group in get_cms_role_group_mappings().items()):
        return True

def authorized_for_cms(request):
    return request.user.is_authenticated and has_cms_role(request.user)


def check_authorization_for_cms_and_admin(request):
    # You are authenticated
    # You want to go to /admin or /cms
    # You have no authorization for any of these paths
    # So: PermissionDenied

    if request.user.is_authenticated and is_admin_path(request) and not authorized_for_admin(request):

        logger.warning(f"User {request.user} is not authorized for /admin or /cms. Assign "
                       f"the proper role in Keycloak for this user.")
        raise PermissionDenied

    if request.user.is_authenticated and is_cms_path(request) and not authorized_for_cms(request):

        logger.warning(f"User {request.user} is not authorized for /admin or /cms. Assign "
                       f"the proper role in Keycloak for this user.")
        raise PermissionDenied


class KeycloakAuthenticationMiddleware:
    """This middleware enforces an authenticated user on any public url. This way, you have to be logged in to
    access public parts. If the user has been logged in through Digid, this middleware will let you through
    anyway.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        check_authorization_for_cms_and_admin(request)

        secure_public_urls = os.getenv("SECURE_PUBLIC_URLS", "True").lower() == "true"

        if (
            is_saml_path(request)
            or is_callback_path(request)
            or is_reload_path(request)
            or not secure_public_urls
        ):
            return self.get_response(request)

        if not request.user.is_authenticated:
            return keycloak_login(request)
        else:
            return self.get_response(request)


class KeycloakCheckTokenMiddleware:
    """Ensures that the token which we got during authentication against Keycloak is still valid."""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = request.user

        if (
            user.is_authenticated
            and not user.is_external
            and not request.path.startswith("/__reload__/events")
        ):
            access_token = request.session["access_token"]
            headers = {"Authorization": f"Bearer {access_token}"}

            refresh_token = request.session["refresh_token"]

            data = {
                "grant_type": "refresh_token",
                "refresh_token": refresh_token,
                "client_id": settings.KEYCLOAK_CLIENT_NAME,
                "client_secret": settings.KEYCLOAK_CLIENT_SECRET,
            }

            try:
                url = (
                    f"{settings.KEYCLOAK_SERVER_URL}/realms/{settings.KEYCLOAK_REALM_NAME}/"
                    f"protocol/openid-connect/token"
                )
                response = requests.post(url, headers=headers, data=data)

                if response.status_code != 200:
                    logger.warning(f"Token for user '{user}' is invalid. Logging out.")

                    logout(request)

                    return keycloak_login(request)
                else:
                    logger.info(f"Token for user '{user}' is valid. Updating tokens.")

                    _json = response.json()

                    request.session["access_token"] = _json["access_token"]
                    request.session["refresh_token"] = _json["refresh_token"]

            except Exception as e:
                logger.exception(
                    f"Error when updating tokens for user '{user}. Error message: {e}"
                )

        response = self.get_response(request)
        return response
