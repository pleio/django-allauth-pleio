ACTIVE_AUTHENTICATION_PROVIDER_KEYCLOAK = "keycloak"
ACTIVE_AUTHENTICATION_PROVIDER_DIGID = "digid"
ACTIVE_AUTHENTICATION_PROVIDER_KEY = "active_authentication_provider"


def is_active_authentication_provider(session, provider):
    return session[ACTIVE_AUTHENTICATION_PROVIDER_KEY] == provider


def set_active_authentication_provider(session, active_provider):
    session[ACTIVE_AUTHENTICATION_PROVIDER_KEY] = active_provider


def clear_active_authentication_provider(session):
    if ACTIVE_AUTHENTICATION_PROVIDER_KEY in session:
        del session[ACTIVE_AUTHENTICATION_PROVIDER_KEY]


def get_active_authentication_provider(session):
    if ACTIVE_AUTHENTICATION_PROVIDER_KEY in session:
        return session[ACTIVE_AUTHENTICATION_PROVIDER_KEY]
    else:
        return None
