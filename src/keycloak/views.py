import logging

from django.contrib.auth.views import LoginView

logger = logging.getLogger(__name__)


class NonKeycloakLoginView(LoginView):
    template_name = "login.html"

    def get_success_url(self):
        return "/cms/"
