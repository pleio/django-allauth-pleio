from django.conf import settings
from django.urls import path

app_name = "keycloak"

if settings.KEYCLOAK_ENABLED:
    urlpatterns = [
        path("accounts/login/", keycloak_login, name="account_login"),
    ]
else:
    urlpatterns = [
        path("accounts/login/", NonKeycloakLoginView.as_view(), name="account_login"),
    ]
