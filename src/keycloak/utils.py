import base64
import json
import jwt
import logging
import requests
from django.conf import settings
from django.contrib.auth import get_user_model, login
from django.contrib.auth import logout as logout_from_django
from django.shortcuts import redirect
from django.urls import reverse
from urllib.parse import urlencode

logger = logging.getLogger(__name__)

KEYCLOAK__ADMIN_SUPERUSER_ROLE_NAME = "ADMIN_SUPERUSER"  # Django User.is_superuser
KEYCLOAK__ADMIN_STAFF_ROLE_NAME = "ADMIN_STAFF"  # Django User.is_staff
KEYCLOAK__CMS_MODERATOR_ROLE_NAME = "CMS_MODERATOR"
KEYCLOAK__CMS_EDITOR_ROLE_NAME = "CMS_EDITOR"

WAGTAIL__EDITORS_GROUP_NAME = "Editors"  # Group (Wagtail)
WAGTAIL__MODERATORS_GROUP_NAME = "Moderators"  # Group (Wagtail)

def base64_encode(data: dict):
    return base64.b64encode(json.dumps(data).encode("utf-8")).decode("utf-8")


def base64_decode(encoded_string):
    return json.loads(base64.b64decode(encoded_string).decode("utf-8"))


def keycloak_callback(request):
    """Called by keycloak to report on the login that is performed in Keycloak.
    """
    code = request.GET.get("code", None)

    host = request.get_host()
    redirect_uri = f"https://{host}" if request.is_secure() else f"http://{host}"
    callback_uri = (
        f"{redirect_uri}/accounts/oidc/{settings.PROVIDER_ALIAS}/login/callback/"  # AAA
    )

    data = {
        "grant_type": "authorization_code",
        "code": code,
        "client_id": settings.KEYCLOAK_CLIENT_NAME,
        "client_secret": settings.KEYCLOAK_CLIENT_SECRET,
        "redirect_uri": callback_uri,
    }

    response = requests.post(
        f"{settings.KEYCLOAK_SERVER_URL}/realms/{settings.KEYCLOAK_REALM_NAME}/protocol/openid-connect/token",
        data=data,
    )
    token_data = response.json()

    access_token = token_data["access_token"]

    decoded_token = jwt.decode(access_token, options={"verify_signature": False})
    email = decoded_token["email"]

    user, created = get_user_model().objects.get_or_create(email=email)

    login(request, user, backend="django.contrib.auth.backends.ModelBackend")

    map_roles_to_groups(user=user, extra_data=decoded_token, keycloak_role_prefix=settings.KEYCLOAK_ROLE_PREFIX)

    # Store access_token and refresh_token in session
    #
    request.session["access_token"] = token_data["access_token"]
    request.session["refresh_token"] = token_data["refresh_token"]

    set_active_authentication_provider(
        request.session, ACTIVE_AUTHENTICATION_PROVIDER_KEYCLOAK
    )

    # Uit de state 'next' halen
    #
    state = request.GET.get("state", None)
    if state:
        _next = base64_decode(state)["next"]
    else:
        _next = "/"

    return redirect(_next)


def keycloak_login(request):
    """Constructs a keycloak login url and redirects to it.
    """
    host = request.get_host()
    redirect_uri = f"https://{host}" if request.is_secure() else f"http://{host}"
    callback_uri = (
        f"{redirect_uri}/accounts/oidc/{settings.PROVIDER_ALIAS}/login/callback/"  # AAA
    )
    if request.path == reverse("logout"):
        state = {"next": "/"}
    else:
        state = {"next": request.path}

    query_params = {
        "client_id": settings.KEYCLOAK_CLIENT_NAME,
        "redirect_uri": callback_uri,
        "response_type": "code",
        "scope": "openid",
        "state": base64_encode(state),
    }

    base_url = f"{settings.KEYCLOAK_SERVER_URL}/realms/{settings.KEYCLOAK_REALM_NAME}/protocol/openid-connect/auth"
    authorization_url = f"{base_url}?{urlencode(query_params)}"

    return redirect(authorization_url)


def logout(request):
    """We need to explicitly logout from keycloak. This method calls the logout for the social account as well as the
    default Django logout. And, for starters, if the user is not anynomous, logs out the user explicitly from Keycloak.
    """

    if not request.user.is_anonymous and settings.KEYCLOAK_ENABLED:
        _logout_from_keycloak(request)

    logout_from_django(request)

    if settings.KEYCLOAK_ENABLED:
        return keycloak_login(request)


def _logout_from_keycloak(request):
    """This method calls the logout endpoint on the authentication server (Keycloak). We have to do this, because
    django-allauth doesn't do it for us."""

    logout_endpoint = (
        f"{settings.KEYCLOAK_SERVER_URL}/realms/{settings.KEYCLOAK_REALM_NAME}/"
        f"protocol/openid-connect/logout"
    )

    refresh_token = request.session.get("refresh_token")

    data = {
        "client_id": settings.KEYCLOAK_CLIENT_NAME,
        "client_secret": settings.KEYCLOAK_CLIENT_SECRET,
        "refresh_token": refresh_token,
    }
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    response = requests.post(logout_endpoint, data=data, headers=headers)

    user = request.user
    if response.ok:
        logger.info(f"Logout of user {user} successful.")
    else:
        logger.warning(
            f"Logout of user {user} failed. Response code from Keycloak logout endpoint {response.status_code}."
        )

    return True


def get_cms_role_group_mappings():
    """Returns all roles that have (some level of) access to /cms."""

    from django.conf import settings

    if not hasattr(settings, "ROLE_GROUP_MAPPING"):
        logger.warning(
            "No role-group mapping found in settings (ROLE_GROUP_MAPPING dictionary). Just adding defaults."
        )

    role_group_mapping = settings.ROLE_GROUP_MAPPING

    default_role_group_mapping = {
        KEYCLOAK__CMS_EDITOR_ROLE_NAME: WAGTAIL__EDITORS_GROUP_NAME,
        KEYCLOAK__CMS_MODERATOR_ROLE_NAME: WAGTAIL__MODERATORS_GROUP_NAME,
    }
    role_group_mapping.update(default_role_group_mapping)

    return role_group_mapping


def map_roles_to_groups(user, extra_data, keycloak_role_prefix):
    from django.contrib.auth.models import Group

    role_group_mapping = get_cms_role_group_mappings()
    group_roles = role_group_mapping.keys()

    # In Keycloak, under [Client scopes > 'roles' > Mappers tab] add a predefined mapper 'realm roles'. Then, enable
    # 'Add to userinfo' for this mapper. This will ensure that the social login gets extra_data["realm_access"]["roles"]
    # set with a list of roles that are assigned in Keycloak for that user. The roles must be
    # defined on the realm level.
    #
    if "realm_access" in extra_data and "roles" in extra_data["realm_access"]:
        roles = extra_data["realm_access"]["roles"]

        # Roles are sent as (for example) 'PLEIO_DGH_LOCAL_ADMIN_STAFF'. 'PLEIO_DGH_LOCAL' is a prefix for the
        # application and environment to which the role applies.
        #

        cleaned_roles = [
            role for role in roles if role.startswith(keycloak_role_prefix)
        ]
        if not cleaned_roles:
            # Users can have no roles, it is perfectly valid.
            #
            return

        cleaned_roles = [
            role.replace(f"{keycloak_role_prefix}_", "") for role in cleaned_roles
        ]

        if KEYCLOAK__ADMIN_SUPERUSER_ROLE_NAME in cleaned_roles:
            user.is_staff = True
            user.is_superuser = True
        elif KEYCLOAK__ADMIN_STAFF_ROLE_NAME in cleaned_roles:
            user.is_staff = True
        else:
            # Explicitly set these properties to False, as the user might exist in Django and be a superuser,
            # but the roles have been removed on the Keycloak side.
            #
            user.is_staff = False
            user.is_superuser = False

        # Map Keycloak roles to Django (including Wagtail) groups.
        #
        user.groups.clear()  # Remove all groups for the user, always start afresh
        for role in cleaned_roles:

            if role in group_roles:
                user.is_cms_user = True

                matching_group_name = role_group_mapping.get(role)
                group_filter = Group.objects.filter(name=matching_group_name)
                if group_filter.count() == 0:
                    raise ValueError(f"Missing group for role {role}")
                else:
                    logger.debug(f"Adding group {group_filter.first()} to user {user}")
                    user.groups.add(group_filter.first())
    else:
        logger.warning(f"Missing 'realm_access' or 'roles'.")